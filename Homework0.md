# Homework 0
## Biography
I am originally from Rapid City, South Dakota and moved to Meridian, Idaho while I was in High School. After graduating with an A.A. of General Studies as a part of a program at my high school, I continued on to ISU and currently on track to graduate next Spring with a B.S in EE and a B.S. in CS. I spend my free time tinkering with hardware and my home server, playing video games, and enjoying a good mountain for snowboarding.

I have a lot of interest in hardware-constrained performance optimization, including utilization of parallel processing techniques and improved code structure.  Machine learning, DevOps, and TDD are also interesting topic areas I would like to investigate.

I would like to learn about both parallelization techniques and existing libraries for parallel processing in this class, especially GPU-related tools and techniques as I have little experience with this type of programming.
 
## Application Problem for Parallel Computing/High Performance
### Batteries Get a Boost from 'Pickled' Electrolytes
https://www.nersc.gov/news-publications/nersc-news/science-news/2018/batteries-get-a-boost-from-pickled-electrolytes/
- What is the scientific or engineering problem being solved? 
  - Scientists were studying the mechanism behind a common additive called trimethylsilyl pohsphite (TMSPi) that "pickles" the cathode surface on lithium-ion batteries to prevent degradation and extends battery life. The product of this pickling is $PF_2OSiMe_3$, which is what actually protects the cathode. Uncovering the "pickling" mechanism is a compuationally intensive process, which requires a supercell (simulation box) 400 atoms in size and two-step, high-level DFT calculations to achieve the necessary electronic convergence using different configurations of molecules and surfaces. 
- How well did the application achieve its scientific / engineering objective? Are simulation results compared to physical results?
  - The results of this simulation were quite sucessful, as it was found that $PF_2OSiMe_3$ strongly binds to the reaction centers on the cathode surface without removing oxygen. This bound molecule can react with TMSPi even more to form an even stronger molecule that permanently caps the reaction centers on the cathode, stabilizing the interface between the liquid and solid electrode. It was noted that this causes battery performance to improve as the additive ages. These results now leave open more windows of research to improve "pickling" further. There was no mention of how these simulation results compare to physical results in the article. 
-   What parallel platform has the application targeted? (distributed vs. shared memory, vector, etc.)
    -  This supercomputer utilizes a distributed memory architecture. The Cori Supercomputer has 2,388 Intel Xeon "Haswell" nodes and 9,688 Intel Xeon Phi "Knight's Landing" nodes, which uses the Cray "Aries" high speed internode network for communication. This architecture is a "mesh" design that connects each node for communication. 
    -   https://www.nersc.gov/users/computational-systems/cori/configuration/burst-buffer/
    -   https://www.nersc.gov/users/computational-systems/cori/programming/
-   What tools were used to build the application? (languages, libraries, etc.)
    -  No tools were mentioned outside of using the Cori Supercomputer and that NERSC provided software support as well. As the actual paper was published in a paid journal I was unable to get a specific answer. However, NERSC highly recommends to use MPI + OpenMP for programming on Cori, and it is plausible this was what was used for this project.
    -   https://www.nersc.gov/users/computational-systems/cori/programming/programming-models/
-   If the application is run on a major supercomputer, where does that computer rank on the Top 500 list?
    -  NERSC Cori Supercomputer, rank 14 as of 06/2019. 
    -  https://www.top500.org/system/178924
- How well did the application perform? How does this compare to the platform's best possible performance?
  - Typical set used 288 processing cores and ran for over 24 hours. 
  - All different configurations took about 60 simulations totalling about 41,500 computing hours
- Does the application "scale" to large problems on many processors? If you believe it has not, what bottlenecks may have limited its performance?
  - Given that the project ran on 288 processing cores with technical help from NERSC, it is likely that this application will scale well on more processors. As I do not have specific technical details, it's hard to definitively say if this application has a ceiling for paralleism. 